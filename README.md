# Google Maps for Flutter with Heatmap Implementation

This is a fork of github.com/SvenSlijkoord's [implementation of heatmaps on google_maps_flutter](https://github.com/SvenSlijkoord/plugins/tree/implement_google_maps_flutter_heatmap).

See the documentation for the [official gooogle_maps_flutter](https://github.com/flutter/plugins/blob/master/packages/google_maps_flutter/google_maps_flutter/README.md). The package is available on [pub.dev](https://pub.dev/packages/google_maps_flutter).

To use, declare in pubspec.yaml:
```
dependencies:
  google_maps_flutter:
    git:
      url: https://gitlab.com/checkitmap/google_maps_flutter.git
```
Import into your .dart file:
```
import 'package:google_maps_flutter/google_maps_flutter.dart';
```
Expanding on an [example](https://pub.dev/packages/google_maps_flutter#sample-usage) usage of google_maps_flutter:
```
// Create a list of WeightedLatLngs
List<WeightedLatLng> list = [
  // Intensity is optional
  new WeightedLatLng(point: LatLng(51.555832, -0.279769) intensity: 1)
];

// Create a Heatmap instance
Heatmap heatmap = new Heatmap(
  heatmapId: heatmapId("uniqueHeatmapId"),
  points: list
);

// Create a Set of Heatmaps
Set<Heatmap> heatmaps = Set.of([
  heatmap,
  // Add any other Heatmap instances here
]);

// Add the Set of Heatmaps to the GoogleMap Widget constructor
GoogleMap(
  onMapCreated: ...,
  initialCameraPosition: CameraPosition( ... ),
  heatmaps: heatmaps, // referencing the set here
),
```
